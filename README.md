 **@BeforeSuite** - metoda oznaczona tą adnotacją zostanie uruchomiona przed uruchomieniem wszystkich testów w dany pakiecie.

 **@AfterSuite** - metoda oznaczona tą adnotacją zostanie uruchomiona po wykonaniu wszystkich metod testowych w pakiecie.

 ##

 **@BeforeTest** - metoda oznaczona tą adnotacją zostanie uruchomiona przed jaką kolwiek metodą testową należąca do klas ujętych między znacznikami `<test>`.

 **@AfterTest** - metoda oznaczona tą adnotacją zostanie uruchomiona po wszystkich metodach testowych należących do klas ujętych między znacznikami `<test>`.

##

**@BeforeClass** - metoda oznaczona tą adnotacją zostanie uruchomiona przed wywołaniem pierwszej metody w bierzącej klasie.

**@AfterClass** - metoda oznaczona tą adnotacją zostanie uruchomiona po wywołaniu ostatniej metody testowej w danej klasie.

##

**@BeforeMethod** - metoda oznaczona tą adnotacją zostanie uruchomiona przed wywołaniem każdej metody testowej.

**@AfterMethod** - metoda oznaczona tą adnotacją zostanie uruchomiona po wywołaniu każdej z metod testowych.

##

**@BeforeGroups(groups = "Group1","Group2",...)** - metoda oznaczona tą adnotacją zostanie uruchomiona przed pierwszą metodą testową należącą do którejkolwiek z wymienionych grup.

**@AfterGroups(groups = "Group1","Group2",...)** - metoda oznaczona tą adnotacją zostanie uruchomiona po wykonaniu ostatniej metody testowej należącej do dowolnej z wymienionych grup.

##

**@Test** - metoda testowa

##
### Dziedziczenie adnotacji
TestNG umożliwia dziedziczenie adnotacji umieszczonych w nadklasach klas testowych, dziedziczone metody **`@Before...`**
są wykonywane zgodnie z chierarchią dziedziczenia, a metody **`@After...`** są wykonywane w odwrotnej kolejności.
##

Poniżej wynik działania adnotacji dla pliku konfiguracyjnego:


```
 <?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="TestNG - annotation" >

    <test name="First" >
        <classes>
            <class name="pl.automaty.testng.tests.user.LoginTest"/>
            <class name="pl.automaty.testng.tests.user.CreateUserTest"/>
        </classes>
    </test>
    <test name="Second">
        <classes>
            <class name="pl.automaty.testng.tests.product.SearchProductTest"/>
            <class name="pl.automaty.testng.tests.user.LogoutTest"/>
        </classes>
    </test>

</suite>
```
Wynik:

```
-------->    Before Suite     <-------
----->  Before Test     <-----
--> Before Class    <--
->  Before Method   <-
LoginTest -> loginExistingUser 	:	1
->  After Method   <-
->  Before Method   <-
LoginTest -> loginNonExistingUser 	:	1
->  After Method   <-
--> After Class     <--
--> Before Class    <--
->  Before Method   <-
CreateUserTest -> createUserWithCorrectData 	:	1
->  After Method   <-
->  Before Method   <-
CreateUserTest -> createUserWithEmptyData 	:	1
->  After Method   <-
->  Before Method   <-
CreateUserTest -> createUserWithIncorrectData 	:	1
->  After Method   <-
--> After Class     <--
----->  After Test     <-----
----->  Before Test     <-----
--> Before Class    <--
->  Before Method   <-
SearchProductTest -> searchEmptyProduct 	:	1
->  After Method   <-
->  Before Method   <-
SearchProductTest -> searchExistingProduct 	:	1
->  After Method   <-
->  Before Method   <-
SearchProductTest -> searchInvalidExistingProduct 	:	1
->  After Method   <-
->  Before Method   <-
SearchProductTest -> searchNonExistingProduct 	:	1
->  After Method   <-
--> After Class     <--
--> Before Class    <--
->  Before Method   <-
LogoutTest -> logoutWhenUserIsLogin 	:	1
->  After Method   <-
->  Before Method   <-
LogoutTest -> logoutWhenUserIsLogout 	:	1
->  After Method   <-
--> After Class     <--
----->  After Test     <-----
------->    After Suite     <--------

===============================================
TestNG - annotation
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```
