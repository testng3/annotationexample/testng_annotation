package pl.automaty.testng.testbases;

public abstract class BaseTest {

    public void info(String methodName) {
        System.out.println(getClass().getSimpleName() + " -> " + methodName + " \t:\t" + Thread.currentThread().getId());
    }
}
