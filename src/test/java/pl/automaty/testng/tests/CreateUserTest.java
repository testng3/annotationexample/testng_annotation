package pl.automaty.testng.tests;

import org.testng.annotations.Test;
import pl.automaty.testng.testbases.BaseTest;

public class CreateUserTest  extends BaseTest {

    @Test
    public void createUserWithCorrectData() {
        info("createUserWithCorrectData");
    }

    @Test
    public void createUserWithIncorrectData() {
        info("createUserWithIncorrectData");
    }

    @Test
    public void createUserWithEmptyData() {
        info("createUserWithEmptyData");
    }
}
