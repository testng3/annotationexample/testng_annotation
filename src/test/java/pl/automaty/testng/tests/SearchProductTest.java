package pl.automaty.testng.tests;

import org.testng.annotations.Test;
import pl.automaty.testng.testbases.BaseTest;

public class SearchProductTest extends BaseTest {

    @Test
    public void searchExistingProduct() {
        info("searchExistingProduct");
    }

    @Test
    public void searchNonExistingProduct() {
        info("searchNonExistingProduct");
    }

    @Test
    public void searchInvalidProduct() {
        info("searchInvalidExistingProduct");
    }

    @Test
    public void searchEmptyProduct() {
        info("searchEmptyProduct");
    }
}
