package pl.automaty.testng.tests;

import org.testng.annotations.Test;
import pl.automaty.testng.testbases.BaseTest;

public class LogoutTest  extends BaseTest {

    @Test
    public void logoutWhenUserIsLogin() {
        info("logoutWhenUserIsLogin");
    }

    @Test
    public void logoutWhenUserIsLogout() {
        info("logoutWhenUserIsLogout");
    }
}
