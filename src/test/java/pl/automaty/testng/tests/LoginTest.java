package pl.automaty.testng.tests;

import org.testng.annotations.Test;
import pl.automaty.testng.testbases.BaseTest;

public class LoginTest extends BaseTest {

    @Test
    public void loginExistingUser() {
        info("loginExistingUser");
    }

    @Test
    public void loginNonExistingUser() {
        info("loginNonExistingUser");
    }
}
